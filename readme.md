- « PrePostPrint », *Code X, 01*, éditions HYX, 2017, Paris
- *EnsadLab invite PrePostPrint, Enjeux des systèmes de publication libres et des outils alternatifs pour la création graphique* , 3 — 4 avril 2018, Ensad, Paris, disponible ici : (http://www.ensadlab.fr/fr/francais-prepostprint/)
- Fauchié, Antoine, « Une chaîne de publication inspirée du web », quaternum.net,in carnet, 2017.
  Disponible ici : (https://www.quaternum.net/2017/03/13/une-chaine-de-publication-inspiree-du-web/)
- Blanc, Julie, Molinié, Christelle, **« Présentation de la publication numérique et imprimée du catalogue des sculptures de la villa romaine de Chiragan »**, *Les Lundis numériques de l’INHA* (Paris, France), 13 janvier 2020. (https://www.youtube.com/watch?v=-bTGBPaJR3o&list=PLsl8NWzVv6T2CQFtBOfnlA_EKLFeCFSUG&index=30)
- Chamaret, Sandra, Horellou, Loïc, « Transmission de données », *Penser, classer, représenter*, Back Office, n°2, B42 et Fork éditions, 2018, Paris.
  Disponible en ligne: (http://www.revue-backoffice.com/numeros/02-penser-classer-representer/chamaret-horellou-transmission-donnees) 
- Open Source Publishing, *Open Source Publishing, Relearn*,<o> future <o>. Texte initialement publié dans la revue [△⋔☼](http://www.bat-editions.net/∆⅄⎈.html), №1, 10/2011, p.35-46. octobre 2011.
  Disponible ici : (http://f-u-t-u-r-e.org/r/02_OSP_Relearn_FR.md)
- Masure, Anthony, « Adobe. Le créatif au pouvoir », in *Strabic*, strabic.fr.
   <https://www.strabic.fr/Adobe-le-creatif-au-pouvoir>
- Taffin, Nicolas, « La vie n’est pas une ‘Creative Suite’ », in *Polylogue*, polylogue.org.
   <https://polylogue.org/la-vie-nest-pas-une-creative-suite/>
- Cardon, Dominique, Culture numérique, coll. « Les petites humanités », éditions SciencesPo les presses, Paris, 2019.
